import java.util.Scanner;

public class JavaPlayground {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int age = 25;
		System.out.println("Anthony is " +age);
		age +=3;
		System.out.println("In Three Years, He will be " +age);
		System.out.println("10.5/3 =" +10.5/3);
		System.out.println("10%3 =" +10%3); //The remainder of 10/3;
		
		/*This is a simple input and output from user
		Scanner in = new Scanner(System.in);
		int Age;
		System.out.println("How old are you?");
		Age = in.nextInt();
		if(Age>= 21)
			System.out.println("Your are old enough to drink");
		else
			System.out.println("You are not old enough to drink");
		//The end of user IO
		*/
		
		//**********************************
		// basic conditionals and comparison
		//Scanner in = new Scanner(System.in); //used to scan for input
		//int height;
		/*int Achild;
		System.out.println("How tall are you in inches?");
		height = in.nextInt();
		System.out.println("How old are you?");
		Achild = in.nextInt();
		if((height>=52) && (Achild>=9))
			System.out.println("You are able to ride the rides");
		else
			System.out.println("You are not able to ride the rides ");
			*/
		
		//*******************************
		/*More interaction with user
		double a;
		double b;
		System.out.println("We will now start to compare two numbers");
		System.out.println("Enter in the first number");
		a=in.nextDouble();
		System.out.println("Enter in second number");
		b = in.nextDouble();
		if(a == b)
			System.out.println("Your numbers are equal");
		else if ( a > b)
			System.out.println("Your first number is larger " +a);
		else
			System.out.println("Your second number is larger "  +b);
		*/
		
		//For loop***************************
		//for(int x = 10; x < 20; x = x+1)
			//System.out.println("The value of x: " +x);
		//end of loop
		
		//For loop for arrays****************
		/*
		int [] numbers = {10, 20, 30, 40, 50};
		for (int x: numbers){//The different from regular for loop
			System.out.print(x);
			System.out.print(",");
		}
		System.out.print("\n");
		String [] names = {"Joe", "Bill", "Tom", "Harry", "Will"};
		for (String name:names){
			System.out.print(name);
			System.out.print(",");
		}*/
		
		/*
		//Class Creation
		class Animal{ 
			
			public void move(){
				System.out.println("Animals can move");
			}
		}
		
		class Dog extends Animal{
			public void move(){ //invokes the Animal move but uses it function within itself
				System.out.println("Dog can walk and run");
			}
		}
		Animal  a = new Animal();
		Animal b = new Dog();
		a.move();
		b.move();
		*/
		
		
	}
		
}

