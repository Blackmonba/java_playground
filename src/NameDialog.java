//Anthony Blackmon
//Name Dialog Textbox which takes input from user
//9/15/2014
import javax.swing.JOptionPane;

public class NameDialog {

	public static void main(String[] args) {
		
		//Prompts user to enter name
		String name = JOptionPane.showInputDialog("What is your name? ");
		
		//Creates the message
		String message = String.format("Welcome %s, to Java Programming", name);
		
		//Displays the message to user
		JOptionPane.showMessageDialog(null, message);

	}

}
