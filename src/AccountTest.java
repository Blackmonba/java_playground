import java.util.Scanner;


public class AccountTest {

	public static void main(String[] args) {
		Account account1 = new Account(25.00);
		Account account2 = new Account(-7.32);
		System.out.println("Account1 Balance: " +account1.getBalance());
		System.out.println("Account2 Balance: " +account2.getBalance());
		
		Scanner input = new Scanner(System.in);
		double depositAmount;
		
		System.out.println("Enter deposit amount for account1: ");
		depositAmount = input.nextDouble();
		System.out.printf("Adding %.2f to account1\n", depositAmount);
		account1.credit(depositAmount);
		
		System.out.printf( "account1 balance: $%.2f\n",account1.getBalance() );
		System.out.printf( "account2 balance: $%.2f\n\n",account2.getBalance() );
		
		System.out.println("Enter deposit amount for account2: ");
		depositAmount = input.nextDouble();
		System.out.printf("Adding %.2f to account2\n", depositAmount);
		account2.credit(depositAmount);
		
		System.out.printf( "account1 balance: $%.2f\n", account1.getBalance() );
		System.out.printf( "account2 balance: $%.2f\n",account2.getBalance() );

	}

}
