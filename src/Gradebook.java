
import java.util.Scanner;

public class Gradebook {
	private String courseName; //Coursename of the gradebook
	
	//method to set CourseName
	public void setCourseName(String name){
		courseName = name;
	}
	
	//method to retrieve CourseName
	public String getCourseName(){
		return courseName;
	}
	
	//method to display Message
	public void displayMessage(){
		System.out.println("Welcome to the gradebook for " +getCourseName());
	}
	
	
//Main Function to run program 	
		public static void main( String[] args ){
			Scanner input = new Scanner(System.in); //Takes input
			Gradebook myGradebook = new Gradebook(); //create myGradebook object
			System.out.println("Please enter the course name");//prompt user
			String nameOfCourse =input.nextLine();
			myGradebook.setCourseName(nameOfCourse);
			myGradebook.displayMessage();
		}
}
