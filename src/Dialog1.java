//Anthony Blackmon
// Basic code that displays Text in Dialog box
//9/15/2014

import javax.swing.JOptionPane;

public class Dialog1 {

	public static void main(String[] args) {
		
		//Displays a dialog box with Welcome to Java Programming text
		JOptionPane.showMessageDialog(null, "Welcome to Java Programming");

	}

}
