
import java.awt.Container;//library for container and content pane
import javax.swing.*; //libary to call JFrame
import java.awt.*;
import java.awt.event.*; //library for event listeners

//JFrame also has a method cant getContentPane which access area instead of window

public class GUI extends JFrame { //GUI inherits properties of JFrame

	private static final int WIDTH = 500; //constant WIDTH = 400
	private static final int HEIGHT = 500; //constant HEIGHT = 300
	private JLabel lengthL, widthL, areaL, perimeterL; //Label names for textfield
	private JTextField lengthTF, widthTF, areaTF, perimeterTF; //Textfields for each input/output
	private JButton calculateB, exitB; //calculate button and exit button
	private CalculateButtonHandler cbHandler; //event handler for calculate
	private ExitButtonHandler ebHandler; //event handler for exit
	
	
	public GUI (){
		
		setTitle("Area and Perimeter of a Rectangle"); //Title of window
		
		lengthL = new JLabel("Enter the length:", SwingConstants.RIGHT);
		widthL = new JLabel("Enter the width:", SwingConstants.RIGHT);
		areaL = new JLabel("Area:", SwingConstants.RIGHT);
		perimeterL = new JLabel("Perimeter:", SwingConstants.RIGHT);
		
		lengthTF = new JTextField(10);
		widthTF = new JTextField(10); 
		areaTF = new JTextField(10); 
		perimeterTF = new JTextField(10);//10 for # of characters
		
		calculateB = new JButton("Calculate"); //create button image
		cbHandler = new CalculateButtonHandler(); //event handler prototype
		calculateB.addActionListener(cbHandler); //event action when button is clicked

		exitB = new JButton("Exit"); //create exit button image
		ebHandler = new ExitButtonHandler(); //event handler prototype
		exitB.addActionListener(ebHandler);//event action when button is clicked
		
		Container pane = getContentPane(); //area of the empty space within the window
		pane.setLayout(new GridLayout(5, 2)); //5 rows 2 columns
		    pane.add(lengthL);// display label
		    pane.add(lengthTF);//display textfield
		    pane.add(widthL);
		    pane.add(widthTF);
		    pane.add(areaL);
		    pane.add(areaTF);
		    pane.add(perimeterL);
		    pane.add(perimeterTF);
		    pane.add(calculateB); //add calculate button to window
		    pane.add(exitB); //add exit button to window
		    
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
		//Class function for calculate button when clicked
		private class CalculateButtonHandler implements ActionListener{
			public void actionPerformed(ActionEvent e){
				double width, length, area, perimeter; //local variables for event
				
				length = Double.parseDouble(lengthTF.getText()); //get text from textfield in length
				
				width = Double.parseDouble(widthTF.getText());  //get text from textfield in width
				
				area = length * width; //area calculation
				
				perimeter = 2 * (length + width); //perimeter calculation
				
				areaTF.setText("" + area); //set value inside of text field
				
				perimeterTF.setText("" + perimeter); //set value inside of perimeter
			}
		}
		
		 private class ExitButtonHandler implements ActionListener{
			public void actionPerformed(ActionEvent e){
				System.exit(0);
			}
		}
		
	public static void main(String[] args) {
		
		GUI mywindow = new GUI();
	

	}

}

